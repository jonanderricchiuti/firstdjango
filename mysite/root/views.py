# Create your views here.
from polls.models import Poll, Choice
from django.shortcuts import render

def index(request):
    latest_poll_list = Poll.objects.order_by('-pub_date')[:5]   
    context = {'latest_poll_list': latest_poll_list}
    return render(request, 'root/index.html', context) 
